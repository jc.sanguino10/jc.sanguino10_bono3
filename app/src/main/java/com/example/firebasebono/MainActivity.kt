package com.example.firebasebono

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream


class MainActivity : AppCompatActivity() {

    var datos: Bitmap? = null
    val CAMERA_REQUEST_CODE = 0
    val storage = FirebaseStorage.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAlmacenar.isEnabled = false
        editText.visibility= View.INVISIBLE

        btnTomarFoto.setOnClickListener {
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        }

        btnAlmacenar.setOnClickListener {
            val arrayBy = ByteArrayOutputStream()
            datos!!.compress(Bitmap.CompressFormat.JPEG, 100, arrayBy)
            val filee = arrayBy.toByteArray()
            var uploadTask = storage.reference.child(editText.text.toString()+".jpg").putBytes(filee)
            uploadTask.addOnFailureListener {
                Toast.makeText(this, "Error al subir la imagen", Toast.LENGTH_SHORT).show()
            }.addOnSuccessListener {
                Toast.makeText(this, "La imagen ha sido subida", Toast.LENGTH_SHORT).show()
            }
        }

        editText.addTextChangedListener( object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnAlmacenar.isEnabled = true
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    datos = data.extras.get("data") as Bitmap
                    imagenCamara.setImageBitmap(datos)
                    //btnAlmacenar.isEnabled = true
                    editText.visibility =View.VISIBLE
                }
            }
            else -> {
                Toast.makeText(this, "Unrecognized request code", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
